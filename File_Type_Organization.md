# File Type Organization
This page will attempt to explain the mechanism of which files are organized into categories.  
You should only see the results of this page via the uploads page on Pitter.

### Why Organize?
Organization allows you on pitter to simply get to the point (or at least that's the attempt) by sorting different uploaded files into categories which will make it easier to find your files.

### Working from the top down
The order of the snippet below matters.  
In this case, the further down the single-dimension array you go down the more "generic" the system will sort them. If a type is undefined, it will be marked as an application and provided for download in application/octet-stream MIME type.

### Why avoiding ambiguity matters
In the goal that is explained above, ambiguity is bad for the service in many ways.
- Languages work incrementally, the further down the array we work the more that we have to assume that the file is an foreign type not known to Pitter which significantly will hang the main thread of which organization works on. This is a huge factor for us as another core focus about the project is speed.
- MIME type guessing isn't always perfect. We have to communicate with your browser when downloading a file to establish what format it should be in.

### Request an extension to be added
As of current development, we're looking to expand to add more categories of which files will be sorted into.  
If you have a proposal of which extensions mime and category should be added: please submit it as an issue.

### Example PHP Snippet of Categories:
```
public static $DEFINITIONS = [
        "SQL" => [
            "extensions" => ["sql", "db"],
            "mimes" => []
        ],
        "EEPROMs" => [
            "extensions" => ["epr", "eeprom"],
            "mimes" => []
        ],
        "RSA/PGP" => [
            "extensions" => ["pub"],
            "mimes" => ["application/pgp"]
        ],
        "Scripts" => [
            "extensions" => ["c", "cpp", "php", "java", "h", "py"],
            "mimes" => ["text/pascal", "text/x-h", "text/x-c", "text/x-java-source"]
        ],
        "Compiled Bytecode" => [
            "extensions" => ["class", "pyc"],
            "mimes" => ["application/java"]
        ],
        "Images" => [
            "extensions" => [],
            "mimes" => ["image/png", "image/jpg", "image/jpeg"]
        ],
        "GIFs" => [
            "extensions" => ["gif", "gifv"],
            "mimes" => ["image/gif"]
        ],
        "Video" => [
            "extensions" => ["mp4", "avi", "mov", "mkv", "flv"],
            "mimes" => ["video/avi", "video/mpeg"]
        ],
        "Audio" => [
            "extensions" => ["mp3", "midi", "flac", "aac", "wav", "aiff"],
            "mimes" => ["audio/mpeg"]
        ],
        "HTML" => [
            "extensions" => ["html", "shtml"],
            "mimes" => ["text/html"]
        ],
        "Archives" => [
            "extensions" => ["gz", "tar", "tar.gz", "rar", "zip", "bzip"],
            "mimes" => ["application/zip", "application/x-compressed", "application/x-rar-compressed", "application/x-gzip"]
        ],
        "PDFs" => [
            "extensions" => ["pdf"],
            "mimes" => ["application/pdf"]
        ],
        "Documents" => [
            "extensions" => ["doc", "docx", "rtf", "txt"],
            "mimes" => ["application/msword", "text/plain"]
        ],
        "Applications" => [
            "extensions" => ["exe"],
            "mimes" => ["application/octet-stream"]
        ],
    ];
```