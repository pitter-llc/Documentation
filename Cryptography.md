# Cryptography
Pitter utilizes Laravel as a MVC framework of which it is built on. This page will discuss the decision of symmetrical encryption per account and how files will be encrypted.

### Philosophy
As a core creator of Pitter, data is everything electronically to many organizations. Whether you are to be considered worried or paranoid with privacy, there is always the thought or chance of another third party company obtaining some form of data about you; A core element of pitter when initially created was to be as strong as possible with cryptography and privacy, with the undeniable right to be forgotten on this service. In the event that something has happened to our service we want to make sure that all data is encrypted to the point of where if there was some form of database breach, that we would suffer minimal impact, which is why cryptography matters to us.

### Choosing a variant of AES
As it currently stands, all files are interpreted as strings and encrypted using `AES-256-CBC`.

### OpenSSL is Hardware Compatible
Certain Intel chips in the modern era of computing come with a specific instruction set by the name of [AES-NI](https://en.wikipedia.org/wiki/AES_instruction_set). We can utilize this to speed up the process of which we decrypt streams to cut latency.

### Libsodium Proposal
As of PHP 7.2, it is the "first language to support modern cryptography standards" as of [RFC:libsodium](https://dev.to/paragonie/php-72-the-first-programming-language-to-add-modern-cryptography-to-its-standard-library). It is a future goal to look at more symmetrical algorithms that have increased security and performance gains over AES-256 CBC, like AES-256-GCM.

### Explanation of why we are avoiding asymmetric cryptography
We understand that public key cryptography and keypairs is a better approach when it comes to storing files, however it would require one of the two implementations:
- Each user has their own private key, which would be stored between a second layer of encryption in the database.
- The server shares the same private key for decryption

### Signing
All encrypted data is HMAC'd.


### The Decision and Containment
Either which route we take on the asymmetric decision we will come to the conclusion that the keys will still be stored on the server. (It is also from our understanding that **RSA/PGP** and **ECC** keypairs do yield a slower performance.)  
However, by using a symmetric key with laravel in the event of a breach in security we could simply perform the following actions in order:

- Decommission the impacted server
- Issue a statement/damage report
- Isolate the network
- Download all encrypted files from long term storage
- Decrypt them with the old key
- Generate a new key
- Encrypt all the files with the new key
- Upload the files back to long term storage
- Apply the key to all the remaining servers
- Purge all File Caches
- Resume Operation

### Asking for recommendations
Pitter is always open to suggestions for better security practices. As it currently stands we're a small team of software engineers with just a decent amount of cryptography experience. If you have anything to suggest about improving our practices for the better of the community of users we operate for, please suggest it as an issue in this repository.