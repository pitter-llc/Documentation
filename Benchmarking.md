# Benchmarking
As of the new code update, and the day of 4/27/2017, the new backend code will now take benchmarks of randomly selected files and measure the time it takes to decrypt, and deliver them to the user.  
I see this coming in handy to make decisions that may affect performance, and provide insight on what I can do to make the network operate faster.

## Random Selection
I plan on proposing a 1/25th chance of a file being randomly benchmarked.

## Scatter/Bubble plot
A visualization of the data will be viewable to the public by the `/details/benchmarks` route, or [here](https://pitterapp.com/details/benchmarks)

## Cluster radius
Determined by the radius of the bubbple, this shows a demand in frequent files, and a general average of where the X(latency) and Y(filesize) slope.  
The bigger the circle, the more accurate of a reading we should have.