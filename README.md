# Pitter Documentation

### Early Disclaimer

Under any circumstance does this documentation come with warranty, meaning that the information documented in this git repository may eventually become obsolete, out of date, depreciated, or even invalid.
The developers of this project reserve the right when available to update this repository with newer documentation of how the backend works, and what is expected of developers to be accomplished.


### Goal
The goal of this repository is to provide throughout documentation of how our backend works, and hopefully provide enough detail to the point where you can write a functioning application on our backend.

### Introduction and Requirements
Below are some basic things to keep in mind while using our documentation, and how it works.
- Each application should have it's own unique API Key and Secret.
- All responses are live generated, and in JSON format.
- Everything you request should be post over HTTPS.
- If you wish to recommend features to be implemented for the API, please submit an issue (we're still learning gitlab, and note that you may not be able to).

### Table of Contents
