# File Cache
This page will attempt to explain the backend decision of caching your files and where AWS comes in for the platform.

## What is the file cache?
In laymen terms, the file cache is just a systematic expression of how files are handled.  
In the event in the future that we ever do decide to scale up the file cache takes the task of downloading the files from a centralized bucket hosted on AWS-S3 and stores the file on disk or the local site until deemed inactive.

### Inactivity
If a file exists in the cache, and has not been viewed after a specific defined amount of time the file will be deleted to make room for other frequently requested files.

The current file lifespan is calculated by the current formula: NI * (((60 ^ 2) * 24) * IP)
- NI (Number of Intervals)
- IP (Interval Period)

As it currently stands, the interval period is 14 days and the number of intervals is simply 1.  

**Why a dynamic formula?**  
In the event that the network expands, it would be easy to change the lifespan for more files to exist in the cache.

### Disk Space Reserve
The file cache has an implementation of bias of which disk space is monitored for the sole purpose of system logs and to make sure that the database is operational while making changes.

In the event that free disk space on the server becomes within 5%, the cache will start to purge the oldest files to make room for more frequent files.

Below is an example calculation in PHP to determine how this is performed:
```
private $directory_prefix = 'cache';
private $reserved_disk_space = 0;
private $reserved_disk_space_percent_in_decimal_form = 0.05;
private $cache_time = 1 * (((60 ^ 2) * 24) * 14);

public function __construct()
{
    $this->reserved_disk_space = disk_total_space(storage_path($this->directory_prefix)) * $this->reserved_disk_space_percent_in_decimal_form;
}
```